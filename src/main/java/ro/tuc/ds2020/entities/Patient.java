package ro.tuc.ds2020.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;



import javax.persistence.Entity;
import javax.persistence.*;
import java.io.Serializable;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="Patient")
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "birthdate", nullable = false)
    private Date birthdate;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "medicalRecord", nullable = false)
    private String medicalRecord;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caregiver")
    private Caregiver caregiver;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<MedicationPlan> medicationPlans;


    public Patient() {

    }

    public Patient(UUID id, String name, String address, Date birthdate, String gender, String medicalRecord, String username, String password, Set<MedicationPlan> medicationPlans) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlans = medicationPlans;

    }

    public Patient(String name, String address, Date birthdate, String gender, String medicalRecord, String username, String password, Set<MedicationPlan> medicationPlans ) {
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlans = medicationPlans;

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }


}
