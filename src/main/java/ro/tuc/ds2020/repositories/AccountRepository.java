package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;


import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {

    @Query(value = "SELECT a " +
            "FROM  Account a " +
            "WHERE a.username = :username ")
    Optional<Account> getAccountByUsername(@Param("username")String username);

}
