package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Patient;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;


import java.util.*;

public interface PatientRepository extends JpaRepository<Patient, UUID> {

    @Modifying
    @Query(value = "UPDATE Patient p SET p.id_caregiver = :id_caregiver WHERE p.id = :id", nativeQuery = true)
    @Transactional
    void assignCaregiver(@Param("id")UUID id, @Param("id_caregiver") UUID id_caregiver);

    @Query(value = "SELECT p " +
    "FROM Patient p " +
    "WHERE p.username = :username ")
    Optional<Patient> getPatientByUsername(@Param("username")String username);

    @Query("SELECT p from Patient p JOIN FETCH p.caregiver c where c.id=:id " )
    List<Optional<Patient>> findPatientsByCaregiver(@Param("id") UUID id);









}
