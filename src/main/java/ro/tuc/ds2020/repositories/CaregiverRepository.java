package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

    @Query(value = "SELECT c " +
            "FROM  Caregiver c " +
            "WHERE c.name = :name ")
    Optional<Caregiver> getCaregiverByName(@Param("name")String name);

    @Query(value = "SELECT c " +
            "FROM  Caregiver c " +
            "WHERE c.username = :username ")
    Optional<Caregiver> getCaregiverByUsername(@Param("username")String username);

}
