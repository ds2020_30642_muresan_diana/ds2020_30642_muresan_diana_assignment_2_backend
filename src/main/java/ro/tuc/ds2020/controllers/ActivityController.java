package ro.tuc.ds2020.controllers;


import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.services.AccountService;
import ro.tuc.ds2020.services.ActivityService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class ActivityController {

    private final ActivityService activityService;
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public ActivityController(SimpMessagingTemplate simpMessagingTemplate, ActivityService activityService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.activityService = activityService;
    }


    public void sendActivity(ActivityDTO activity)   {

        this.simpMessagingTemplate.convertAndSend("/topic/activities",activity);
    }


    @GetMapping()
    public ResponseEntity<List<ActivityDTO>> getActivity() {
        List<ActivityDTO> dtos = activityService.findActivities();
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }

    @PostMapping()
    public ResponseEntity<UUID> insert(@Valid @RequestBody ActivityDTO activityDTO) {
        UUID activityID = activityService.insert(activityDTO);
        return new ResponseEntity<>(activityID, HttpStatus.CREATED);
    }






}
