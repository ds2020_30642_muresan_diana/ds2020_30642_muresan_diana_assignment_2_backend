package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;

import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<List<AccountDTO>> getAccount() {
        List<AccountDTO> dtos = accountService.findAccounts();
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }

    @PostMapping()
    public ResponseEntity<UUID> insertAccount(@Valid @RequestBody AccountDetailsDTO accountDTO) {
        UUID accountID = accountService.insert(accountDTO);
        return new ResponseEntity<>(accountID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/get-by-username/{username}")
    public ResponseEntity<AccountDetailsDTO> getAccountByUsername(@PathVariable("username") String username) {
        AccountDetailsDTO dto = accountService.getAccountByUsername(username);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<AccountDetailsDTO> updateAccount(@Valid @RequestBody AccountDetailsDTO accountDTO) {
        accountService.updateAccountById(accountDTO);
        return new ResponseEntity<>(accountDTO, HttpStatus.OK);
    }


}
