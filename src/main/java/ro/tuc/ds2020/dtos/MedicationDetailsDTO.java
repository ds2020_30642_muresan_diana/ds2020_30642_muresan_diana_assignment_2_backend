package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicationDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String sideEffects;
    @NotNull
    private String dosage;

    public MedicationDetailsDTO() {

    }

    public MedicationDetailsDTO(@NotNull  String name, @NotNull String sideEffects, @NotNull String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(UUID id,  @NotNull String name, @NotNull String sideEffects, @NotNull String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDetailsDTO that = (MedicationDetailsDTO) o;
        return name.equals(that.name) &&
                sideEffects.equals(that.sideEffects) &&
                dosage.equals(that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }
}
